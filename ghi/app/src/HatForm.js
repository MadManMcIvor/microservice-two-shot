import React from 'react';

class HatForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fabric: '',
      style_name: '',
      color: '',
      picture: '',
      locations: [],
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFabricChange = this.handleFabricChange.bind(this);
    this.handleStyleNameChange = this.handleStyleNameChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handlePictureChange = this.handlePictureChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);

  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ locations: data.locations });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.locations;
    console.log(data);
    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const attendeeResponse = await fetch(hatUrl, fetchOptions);
    if (attendeeResponse.ok) {
      this.setState({
        fabric: '',
        style_name: '',
        color: '',
        picture: '',
        location: '',
      });
    }
  }

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value });
        }

    handleStyleNameChange(event) {
        const value = event.target.value;
        this.setState({ style_name: value });
        }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value });
        }

    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({ picture: value });
        }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value });
        }

  render() {
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Hat!</h1>
            <form onSubmit={this.handleSubmit} id="create-hat-form">
              
              <div className="form-floating mb-3">
                <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleStyleNameChange} value={this.state.style_name} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control"/>
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              
              <div className="form-floating mb-3">
                <input onChange={this.handlePictureChange} value={this.state.picture} placeholder="Max Picture" required type="text" name="picture" id="picture" className="form-control"/>
                <label htmlFor="picture">Picture</label>
              </div>

              <div className="mb-3">
                <select onChange={this.handleLocationChange} value={this.state.location} required name="location" id="location" className="form-select">
                  <option value="">Choose a Location</option>
                  {this.state.locations.map(location => {
                        return (
                            <option key={location.closet_name} value={location.closet_name}>
                                {location.closet_name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Add</button>
            </form>

          </div>
        </div>
      </div>
      );
    }
  }

export default HatForm;