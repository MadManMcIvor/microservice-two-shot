from django.db import models
from django.urls import reverse



class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)


# Create your models here.
class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture = models.URLField(null=True, blank=True)
    location = models.ForeignKey( 
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True, 
        blank=True
    )

    def __str__(self):
        return f"{self.color} {self.style_name}"

    def get_api_url(self):
        return reverse("api_list_hats", kwargs={"pk": self.pk})


