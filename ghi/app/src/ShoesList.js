import { Link } from 'react-router-dom';

function ShoesList(props) {
    return (
      <div className="container">
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-end">
                <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Shoe</Link>
        </div>
        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Manufacturer</th>
                <th>Mode Name</th>
                <th>Color</th>
                <th>Picture</th>
              </tr>
            </thead>
            <tbody>
              {props.shoes.map(shoe => {
                return (
                  <tr key={shoe.id}>
                    <td>{ shoe.manufacturer }</td>
                    <td>{ shoe.model_name }</td>
                    <td>{ shoe.color }</td>
                    <td>
                      <img src= {shoe.picture} alt="" />
                    </td>
                    <td>
                    <button type="button" className="btn btn-danger" onClick={() => deleteItem(shoe)}>delete</button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }

async function deleteItem(shoe) {
  const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}`;
  const fetchOptions = {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json',
    },
  };
  await fetch(shoeUrl, fetchOptions);
  window.location.reload(true);
}



export default ShoesList;