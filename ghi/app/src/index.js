import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


// async function loadHats() {
//   const response = await fetch('http://localhost:8090/api/hats/');
//   if (response.ok) {
//     const data = await response.json();
//     loadShoes(data);
//   } else {
//     console.error(response);
//   }
// }

// async function loadShoes(data) {
//   const response2 = await fetch('http://localhost:8080/api/shoes/');
//   if (response2.ok) {
//     const data2 = await response2.json();
//     root.render(
//       <React.StrictMode>
//         <App hats={data.hats} shoes={data2.shoes} />
//       </React.StrictMode>
//     );
//   } else {
//     console.error(response2);
//   }
// }

// loadHats();


//Code above but refactored
async function loadHatsAndShoes() {
  const hatResponse = await fetch('http://localhost:8090/api/hats/');
  const shoeResponse = await fetch('http://localhost:8080/api/shoes/');
  if (hatResponse.ok && shoeResponse.ok) {
    const hatData = await hatResponse.json();
    const shoeData = await shoeResponse.json();
    root.render(
      <React.StrictMode>
        <App hats={hatData.hats} shoes={shoeData.shoes} />
      </React.StrictMode>
    );
  } else {
    console.error(hatResponse);
    console.error(shoeResponse);
  }
}

loadHatsAndShoes();
