# Wardrobify

Team:
* Person 1 - Which microservice?
* Alex McIvor - I'm handling Hats
* Person 2 - Which microservice?
* Julie Liao - Shoes 

## Design
Link to Notion Page: https://curly-ceres-7a9.notion.site/Wardrobify-7341e91117b74ae3a6f26afc6a335b8f

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice


## How to use
Git clone the repo.
Fire up your docker desktop app
run: docker volume create pgdata
run: docker-compose build
run: docker-compose up
Add loctions and bins using the Insomnia app"
    Link to Notion Page with pictures for Insomina setup: https://curly-ceres-7a9.notion.site/Wardrobify-7341e91117b74ae3a6f26afc6a335b8f
Go to localhost:3000
Click around, add some shoes and hats, and enjoy!