import React from 'react';

class CreateShoesForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturer: "",
      model_name: "",
      color: '',
      picture: '',
      bins: [],
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    this.handleModelNameChange = this.handleModelNameChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handlePictureChange = this.handlePictureChange.bind(this);
    this.handleBinChange = this.handleBinChange.bind(this);

  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ bins: data.bins });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.bins;
    console.log(data);
    
    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const personResponse = await fetch(shoeUrl, fetchOptions);
    if (personResponse.ok) {
        const cleared = {
            manufacturer: "",
            model_name: "",
            color: '',
            picture: '',
            bin: "",
        };
        this.setState(cleared);
    }
  }

    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value });
        }

    handleModelNameChange(event) {
        const value = event.target.value;
        this.setState({ model_name: value });
        }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value });
        }

    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({ picture: value });
        }

    handleBinChange(event) {
        const value = event.target.value;
        this.setState({ bin: value });
        }

  render() {
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Shoes!</h1>
            <form onSubmit={this.handleSubmit} id="create-shoe-form">
              
              <div className="form-floating mb-3">
                <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleModelNameChange} value={this.state.model_name} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              
              <div className="form-floating mb-3">
                <input onChange={this.handlePictureChange} value={this.state.picture} placeholder="Max Picture" required type="text" name="picture" id="picture" className="form-control"/>
                <label htmlFor="picture">Picture</label>
              </div>

              <div className="mb-3">
                <select onChange={this.handleBinChange} value={this.state.bin} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a bin</option>
                  {this.state.bins.map(bin => {
                        return (
                            <option key={bin.id} value={bin.closet_name}>
                                {bin.closet_name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Add</button>
            </form>

          </div>
        </div>
      </div>
      );
    }
  }

export default CreateShoesForm;