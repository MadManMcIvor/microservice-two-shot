import { Link } from 'react-router-dom';


function HatList(props) {
  return (
      <div className="container">
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-end m-4">
              <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Hat!</Link>
        </div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Fabric</th>
                <th>Style Name</th>
                <th>Color</th>
            </tr>
            </thead>
            <tbody>
            {props.hats.map(hat => {
                console.log(hat);
                return (
                <>
                <tr key={hat.id}>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.style_name }</td>
                    <td>{ hat.color }</td>
                    <td><button type="button" className="btn btn-danger" onClick={() => deleteItem(hat)}>delete</button> </td>
                </tr>
                </>
                );
            })}
            </tbody>
        </table>
      </div>
    );
  }

async function deleteItem(hat) {
    const hatUrl = `http://localhost:8090/api/hats/${hat.id}`;
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    };
   await fetch(hatUrl, fetchOptions);
   window.location.reload(true);
  }



  export default HatList;