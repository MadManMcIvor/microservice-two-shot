import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './Hats';
import HatForm from './HatForm';
import ShoesList from "./ShoesList"
import CreateShoesForm from './CreateShoesForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="shoes" >
            <Route path="" element={<ShoesList shoes={props.shoes}/>} />
            <Route path="new" element={<CreateShoesForm/>} />
          </Route>
          <Route path="hats" >
            <Route path="" element={<HatList hats={props.hats} />} />
            <Route path="new" element={<HatForm/>} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
